#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <time.h>

void do_ls(char *dirName);   
void do_stat(char *fileName);
void show_info(char *fileName, struct stat *info_p);

int main(){
    do_ls( "." );
    return 0;
}

void do_ls(char *dirName){
    DIR *dir;
    struct dirent *dirent_p;
    if((dir = opendir(dirName)) == NULL ){
     fprintf(stderr,"cannot open %s.n", dirName);
     do_stat(dirName);
    }else{
      while((dirent_p = readdir(dir)) != NULL){
      do_stat(dirent_p->d_name);
        }
        closedir(dir);
   }
}

void do_stat(char *fileName){
  struct stat info;
   if(stat(fileName, &info) == -1){
        perror(fileName);
    }else{
      show_info(fileName, &info);
  }
}

void show_info(char *fileName, struct stat *info_p){
  printf("%s  " , fileName);
}
