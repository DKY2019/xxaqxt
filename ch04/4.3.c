#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#define N 4
int A[N][N], sum[N];


int total = 0;
pthread_mutex_t *m;

void *func(void *arg)
{
        int i,row,sum =0 ;
        pthread_t tid = pthread_self();
        row = (int)arg;
        printf("Thread %d [%lu] computes sum of row %d\n",row,tid,row);
        for(i=0;i<N; i++)
                sum += A[row][i];
        printf("Thread %d [%lu] update total with %d : ",row,tid,sum);
        pthread_mutex_lock(m);
	 total += sum;
	pthread_mutex_unlock(m);
	printf("total = %d\n",total);
}
        int main(int argc, char *argv[])
{
        pthread_t thread[N];
        int i,j,r;
        void *status;
        printf("Main: initialize A matrix\n");
        for(i=0; i<N;i++){
                sum[i] = 0;
                for(j=0;j<N;j++){
                        A[i][j]=i*N+j+1;
                        printf("%4d ",A[i][j]);
                }
                printf( "\n" );
        }
	m = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t));
	pthread_mutex_init(m,NULL);
        printf ("Main: create %d threads\n",N);
        for(i=0;i<N;i++) {
                pthread_create(&thread[i],NULL,func,(void *)i);
        }
        printf("Main: try to join with thread\n");
        for(i=0; i<N; i++) {
                pthread_join(thread[i],&status);
                printf("Main: joined with %d [%lu]: status=%d\n",i,thread[i],
                                (int)status);
        }
        printf ("Main:tatal = %d\n",total );
	pthread_mutex_destroy(m);
        pthread_exit(NULL);
}
