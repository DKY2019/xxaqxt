#include <stdio.h>
#include <stdlib.h>
#include <string.h>
 
int main ()
{
    FILE *fp;
    char Filename[50], ch;
    int count = 0;
    printf("Enter PathName:");
    scanf("%s", Filename);
    if((fp = fopen(Filename, "r")) == NULL)
    {
        printf("File open failed!\n");
        exit(0);
    }
    while(!feof(fp))
    {
        ch = fgetc(fp);
        if(ch != EOF)
        {
            if(ch >= 'a' && ch <= 'z')
            {
                ch -= 32;
                count ++;
            }
            printf("%c", ch);
        }
    }
    printf("\ntotal change : %d\n", count);
    return 0;
}
