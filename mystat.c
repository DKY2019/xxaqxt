#include<stdio.h>
#include<time.h>
#include<sys/stat.h>
#include<unistd.h>
#include<sys/types.h>
#include<errno.h>
#include<stdlib.h>
 
int main(int argc, char *argv[])
{
    struct stat buf;
    if(argc!=2)
    {
        perror("put in <filename>\n");
        exit(1);
    }
 
    if( stat(argv[1], &buf) ==-1 )
    {
        perror("stat fail");
        exit(1);
    }
    
    
    printf("\n");
    printf("  file name:    %s\n",argv[1]);
    printf("size of file:                     %d\n",buf.st_size*8);
    printf("number of blocks allocated is:    %d\n",buf.st_blocks);
    printf("blocksize for filesystem I/O is:  %d\n",buf.st_blksize);
    printf("device is:                        %d\n",buf.st_dev);
    printf("inode is :                        %d\n",buf.st_ino);    
    printf("number of hard links is:          %d\n",buf.st_nlink);    
    printf("mode is:                          %o\n",buf.st_mode);
    printf("user ID of owner is:              %d\n",buf.st_uid);
    printf("group ID of owner is:             %d\n",buf.st_gid);
    printf("device type (if inode device )is: %d\n",buf.st_rdev);
  

    printf("\n\n");
    printf("time of lasst access is: %s",ctime(&buf.st_atime));
    printf("time of last modification is: %s",ctime(&buf.st_mtime));
    printf("time of last chage is: %s",ctime(&buf.st_ctime));
 
    return 0;
}

