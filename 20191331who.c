#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <utmp.h>
#include <fcntl.h>
#include <time.h>


void cutime(time_t);
void usrinfo(struct utmp *);

int main()
{
        struct utmp     utbuf;
        int             utmpfd;

        if ( (utmpfd = open(UTMP_FILE, O_RDONLY)) == -1 ){
                perror(UTMP_FILE);
                exit(1);
        }

        while( read(utmpfd, &utbuf, sizeof(utbuf)) == sizeof(utbuf) )
                usrinfo( &utbuf );
        close(utmpfd);
        return 0;
}

void usrinfo( struct utmp *utbufp )
{
        if ( utbufp->ut_type != USER_PROCESS )
                return;

        printf("%-8.8s", utbufp->ut_name);
        printf(" ");
        printf("%-8.8s", utbufp->ut_line);
        printf(" ");
        cutime( utbufp->ut_time );
}

void cutime(time_t t)
{
        char str_t[26] = {0};
        struct tm* p_time = localtime(&t);
        strftime(str_t, 26, "%Y-%m-%d %H:%M:%S\n", p_time);
        printf ("    %s\n", str_t);
}
