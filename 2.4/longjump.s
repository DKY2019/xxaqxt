	.file	"longjump.c"
	.comm	env,200,32
	.section	.rodata
	.align 8
.LC0:
	.string	"call setjmp to save environment"
.LC1:
	.string	"normal return"
	.align 8
.LC2:
	.string	"back to main() via long jump, r = %d a = %d\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	$100, -8(%rbp)
	movl	$.LC0, %edi
	call	puts
	movl	$env, %edi
	call	_setjmp
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	jne	.L6
	movl	$0, %eax
	call	A
	movl	$.LC1, %edi
	call	puts
	jmp	.L4
.L6:
	movl	-8(%rbp), %edx
	movl	-4(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC2, %edi
	movl	$0, %eax
	call	printf
.L4:
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.section	.rodata
.LC3:
	.string	"enter A()"
.LC4:
	.string	"exit A()"
	.text
	.globl	A
	.type	A, @function
A:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$.LC3, %edi
	call	puts
	movl	$0, %eax
	call	B
	movl	$.LC4, %edi
	call	puts
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	A, .-A
	.section	.rodata
.LC5:
	.string	"enter B()"
.LC6:
	.string	"long jump? (y|n) "
.LC7:
	.string	"exit B()"
	.text
	.globl	B
	.type	B, @function
B:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$.LC5, %edi
	call	puts
	movl	$.LC6, %edi
	movl	$0, %eax
	call	printf
	call	getchar
	cmpl	$121, %eax
	jne	.L9
	movl	$1234, %esi
	movl	$env, %edi
	call	longjmp
.L9:
	movl	$.LC7, %edi
	call	puts
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	B, .-B
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
