package 密码工程;
import java.nio.charset.Charset;

import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.SHA3Digest;
import org.bouncycastle.crypto.digests.SHAKEDigest;
import org.bouncycastle.util.encoders.Hex;

public class SHA {

	    // SHA3-224 算法
	    public static String sha3224(byte[] bytes) {
	        Digest digest = new SHA3Digest(224);
	        digest.update(bytes, 0, bytes.length);
	        byte[] rsData = new byte[digest.getDigestSize()];
	        digest.doFinal(rsData, 0);
	        return Hex.toHexString(rsData);
	    }

	    // SHA3-256 算法
	    public static String sha3256(byte[] bytes) {
	        Digest digest = new SHA3Digest(256);
	        digest.update(bytes, 0, bytes.length);
	        byte[] rsData = new byte[digest.getDigestSize()];
	        digest.doFinal(rsData, 0);
	        return Hex.toHexString(rsData);
	    }

	    // SHA3-384 算法
	    public static String sha3384(byte[] bytes) {
	        Digest digest = new SHA3Digest(384);
	        digest.update(bytes, 0, bytes.length);
	        byte[] rsData = new byte[digest.getDigestSize()];
	        digest.doFinal(rsData, 0);
	        return Hex.toHexString(rsData);
	    }

	    // SHA3-512 算法
	    public static String sha3512(byte[] bytes) {
	        Digest digest = new SHA3Digest(512);
	        digest.update(bytes, 0, bytes.length);
	        byte[] rsData = new byte[digest.getDigestSize()];
	        digest.doFinal(rsData, 0);
	        return Hex.toHexString(rsData);
	    }

	    // SHAKE-128 算法
	    public static String shake128(byte[] bytes) {
	        Digest digest = new SHAKEDigest(128);
	        digest.update(bytes, 0, bytes.length);
	        byte[] rsData = new byte[digest.getDigestSize()];
	        digest.doFinal(rsData, 0);
	        return Hex.toHexString(rsData);
	    }

	    // SHAKE-256 算法
	    public static String shake256(byte[] bytes) {
	        Digest digest = new SHAKEDigest(256);
	        digest.update(bytes, 0, bytes.length);
	        byte[] rsData = new byte[digest.getDigestSize()];
	        digest.doFinal(rsData, 0);
	        return Hex.toHexString(rsData);
	    }

	    public static void main(String[] args) {
	        byte[] bytes = "20191331lyx".getBytes(Charset.forName("UTF-8"));
	        String sha3224 = sha3224(bytes);
	        System.out.println("sha3-224:" + sha3224 + ",lengh=" + sha3224.length());
	        String sha3256 = sha3256(bytes);
	        System.out.println("sha3-256:" + sha3256 + ",lengh=" + sha3256.length());
	        String sha3384 = sha3384(bytes);
	        System.out.println("sha3-384:" + sha3384 + ",lengh=" + sha3384.length());
	        String sha3512 = sha3512(bytes);
	        System.out.println("sha3-512:" + sha3512 + ",lengh=" + sha3512.length());
	        String shake128 = shake128(bytes);
	        System.out.println("shake-128:" + shake128 + ",lengh=" + shake128.length());
	        String shake256 = shake256(bytes);
	        System.out.println("shake-256:" + shake256 + ",lengh=" + shake256.length());
	    }
}

