```
class Md5Test {
    public static void main(String[] args) throws NoSuchAlgorithmException {
        /* 得注意加密后字符串的大小写 */
        // 待加密字符
        String originalStr = "111111";
        System.out.println(String.format("待加密字符: %s", originalStr));

        // 已加密字符
        String alreadyDigestStr = "96E79218965EB72C92A549DD5A330112";
        System.out.println(String.format("已加密字符: %s", alreadyDigestStr));

        /* jdk 实现 */
        System.out.println("--------------jdk 实现-----------------");
        // 获取信息摘要对象
        MessageDigest md5 = MessageDigest.getInstance("MD5");

        // 完成摘要
        byte[] digest = md5.digest(originalStr.getBytes());

        // 将摘要转换成 16 进制字符 (大写)
        String javaMd5Str = DatatypeConverter.printHexBinary(digest);

        System.out.println(String.format("%s 加密结果：%s", originalStr, javaMd5Str));

        // 匹配验证
        System.out.println(String.format("验证结果：%b", Objects.equals(javaMd5Str, alreadyDigestStr)));

        /* Apache commons-codec 实现 */
        System.out.println("---------------Apache commons-codec 实现------------");
        // 小写
        String apacheMd5Str = DigestUtils.md5Hex(originalStr.getBytes()).toUpperCase();
        System.out.println(String.format("%s 加密结果：%s", originalStr, apacheMd5Str));
        System.out.println(String.format("验证结果：%b", Objects.equals(apacheMd5Str, alreadyDigestStr)));

        /* spring提供 */
        System.out.println("-----------spring提供-----------");
        // 小写
        String springMd5Str = org.springframework.util.DigestUtils.md5DigestAsHex(originalStr.getBytes()).toUpperCase();
        System.out.println(String.format("%s 加密结果：%s", originalStr, springMd5Str));
        System.out.println(String.format("验证结果：%b", Objects.equals(springMd5Str, alreadyDigestStr)));
    }

}
```

```
class SHATest {
    public static void main(String[] args) throws NoSuchAlgorithmException {
        // 待加密字符
        String originalStr = "111111";
        System.out.println(String.format("待加密字符: %s", originalStr));

        // 已加密字符
        String alreadyDigestStr = "3D4F2BF07DC1BE38B20CD6E46949A1071F9D0E3D";
        System.out.println(String.format("已加密字符: %s", alreadyDigestStr));

        /* jdk 实现 */
        // 获取信息摘要对象 （SHA1），另外还有 SHA-224、SHA-256、SHA-384、SHA-512
        MessageDigest md5 = MessageDigest.getInstance("SHA");

        // 完成摘要
        byte[] digest = md5.digest(originalStr.getBytes());

        // 将摘要转换成 16 进制字符 (大写)
        String javaShaStr = DatatypeConverter.printHexBinary(digest);

        System.out.println(String.format("%s 加密结果：%s", originalStr, javaShaStr));

        // 匹配验证
        System.out.println(String.format("验证结果：%b", Objects.equals(javaShaStr, alreadyDigestStr)));

        /* Apache commons-codec 实现 */
        String apacheShaStr = DigestUtils.sha1Hex(originalStr.getBytes()).toUpperCase();
        System.out.println(String.format("%s 加密结果：%s", originalStr, apacheShaStr));
        System.out.println(String.format("验证结果：%b", Objects.equals(apacheShaStr, alreadyDigestStr)));

        /* SHA256 */
        System.out.println(DigestUtils.sha256Hex(originalStr.getBytes()));
        /* SHA384 */
        System.out.println(DigestUtils.sha384Hex(originalStr.getBytes()));
        /* SHA512 */
        System.out.println(DigestUtils.sha512Hex(originalStr.getBytes()));
    }
}
```

```
class MacTest {
	public static void main(String[] args) throws NoSuchAlgorithmException, DecoderException, InvalidKeyException {
		// 待加密字符
		String originalStr = "111111";
		System.out.println(String.format("待加密字符: %s", originalStr));

		// 已加密字符
		String alreadyDigestStr = "74B31DBA9E16CCF752B294A18271BC6A";
		System.out.println(String.format("已加密字符: %s", alreadyDigestStr));

		/* jdk 实现 */
		// 初始化 KeyGenerator, jdk 提供 HmacMD5、HmacSHA1、HmacSHA256、HmacSHA384和 HmacSHA512 四种算法
//		KeyGenerator keyGenerator = KeyGenerator.getInstance("HmacMD5");
		// 产生密钥
//		SecretKey secretKey = keyGenerator.generateKey();
		// 默认密钥
		// byte[] defaultKey = secretKey.getEncoded();
		// 自定义密钥
		byte[] myKey = Hex.decodeHex(new char[]{'a','a','a','a','a','a','a','a','a','a'});
		// 还原密钥
		SecretKey restoreSecretKey = new SecretKeySpec(myKey, "HmacMD5");
		// 实例化 MAC
		Mac mac = Mac.getInstance(restoreSecretKey.getAlgorithm());
		// 初始化 MAC
        mac.init(restoreSecretKey);
        //执行摘要
		byte[] hmacMD5Bytes = mac.doFinal(originalStr.getBytes());
        String encodeHexString = Hex.encodeHexString(hmacMD5Bytes).toUpperCase();
        System.out.println(String.format("%s 加密结果：%s", originalStr, encodeHexString));
		System.out.println(String.format("验证结果：%b", Objects.equals(encodeHexString, alreadyDigestStr)));


		/* apache 实现 */
        // HmacMD5
        HmacUtils hmacMd5 = new HmacUtils(HmacAlgorithms.HMAC_MD5, myKey);
        String apacheHmacMd5 = hmacMd5.hmacHex(originalStr.getBytes()).toUpperCase();
        System.out.println(String.format("%s 加密结果：%s", originalStr, apacheHmacMd5));
        System.out.println(String.format("验证结果：%b", Objects.equals(apacheHmacMd5, alreadyDigestStr)));

        // HmacSHA1
        HmacUtils hmacSha1 = new HmacUtils(HmacAlgorithms.HMAC_SHA_1, KeyGenerator.getInstance(HmacAlgorithms.HMAC_SHA_1.getName()).generateKey().getEncoded());
        String apacheHmacHex1 = hmacSha1.hmacHex(originalStr.getBytes()).toUpperCase();
        System.out.println(String.format("%s 加密结果：%s", originalStr, apacheHmacHex1));

        // HmacSHA256
        HmacUtils hmacSha256 = new HmacUtils(HmacAlgorithms.HMAC_SHA_256, KeyGenerator.getInstance(HmacAlgorithms.HMAC_SHA_256.getName()).generateKey().getEncoded());
        String apacheHmacSha256 = hmacSha256.hmacHex(originalStr.getBytes()).toUpperCase();
        System.out.println(String.format("%s 加密结果：%s", originalStr, apacheHmacSha256));

        // HmacSHA384 , 类似上面

        // HmacSHA512， 类似上面
    }
}
```