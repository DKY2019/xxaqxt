#ifndef C_CACHE_H
#define C_CACHE_H

struct TRingBuf {
    char *buf;
    char *end_pos_;
    char *read_pos_;
    char *write_pos_;
    //int data_size_;
    int data_write_size_;
    int data_read_size_;
};
void TRingBufCreate(struct TRingBuf *self, void *buf, int buf_size);//construct
void TRingBufWrite(struct TRingBuf *self, void *data, int data_size);
void TRingBufRead(struct TRingBuf *self, void *buf, int buf_size);
void TRingBufClear(struct TRingBuf *self);//destruct
int TRingBufSize(struct TRingBuf *self);
int TRingBufFreeSize(struct TRingBuf *self);
int TRingBufDataSize(struct TRingBuf *self);

#endif

