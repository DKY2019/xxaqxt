#include "stdio.h"
void main()
{
FILE* fin = NULL;//输入文件
FILE* fout = NULL;//输出文件
char c;
fin = fopen("A.txt", "r");
if(fin == NULL)
{
printf("输入文件打开错误!\n");
return;
}
fout = fopen("out.txt", "w");
if(fout == NULL)
{
printf("输出文件打开错误!\n");
return;
}
c = fgetc(fin);
while(c != EOF)
{
fputc(c, fout);
printf("%c", c);//对于这句来说，如果是非中文，输出到屏幕会有问题的！
c = fgetc(fin);
}
fclose(fin);
fclose(fout);
printf("\n输入输出结束!\n");
}